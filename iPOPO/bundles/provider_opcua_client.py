from pelix.ipopo.decorators import ComponentFactory, Requires, Provides, Instantiate, Property, Validate, Invalidate
from opcua import Client
from opcua import ua
from opcua.client.ua_client import UaClient
from threading import Thread
import pelix.services
import pelix.framework
import logging, time, json, threading

@ComponentFactory('opcua-client-provider-factory')
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Requires('_rest_api_svc', 'servlet_rest_api_service')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Provides('opcua_client_service', 'opcua_client_service_controller')
@Instantiate('provider_opcua_client')
class OPCUA_Client_Service(object):

    def __init__(self):
        """
        This method is called upon initiation of this script
        """
        self._event = None
        self._handler = None
        self._root_node = None
        self._is_connected = False
        self._opcua_client_subscriptions = None
        self._opcua_client_provider_config = self.read_config()
        self._opcua_client_subscription_period = None
        self._service_registration = None
        self._context = None
        self._service_name = 'opcua_client_service'
        self.rcn_thread = None
        self._rest_api_svc = None

    @Validate
    def validate(self, context):
        self.opcua_client_service_controller = False
        """
        Function to create an OPCUA Client
        Creates an OPC UA Client and gets the root node
        Tries to reconnect in case of connection failure
        data -- data read from config
        """

        url = self._opcua_client_provider_config['address']
        self._client = Client(url, timeout=5)
        self._handler = self
        self._context = context
        t1 = Thread(target=self.try_connection)
        t1.daemon = True
        t1.start()
        #self.try_connection()
        logging.info('OPCUA Provider validated')

    @Invalidate
    def Invalidate(self, context):
        logging.info('Provider invalidated')

    def try_connection(self):
        if self._is_connected == False:
            while True:
                try:
                    self._client.connect()
                    self._root_node = self._client.get_root_node()
                    self._is_connected = True
                    self._register_service()
                    connection_thread = Thread(target=self.check_connection_status)
                    connection_thread.daemon = True
                    connection_thread.start()
                    return
                except Exception as e:
                    logging.error(e)
                    logging.error('Waiting for server to become available')
                    self._is_connected = False
                    self._unregister_service()
                    time.sleep(1)


    def _register_service(self):
        if not self._service_registration:
            self._service_registration = self._context.register_service(self._service_name, self, {})

    def _unregister_service(self):
        if self._service_registration:
            logging.info('OPCUA provider invalidated')
            self._context.get_framework().unregister_service(
                self._service_registration)
            self._service_registration = None

    def check_connection_status(self):
        """
        Checks the connection status of the opcua client
        If it's disconnected, this method sends an event
        to the iPOPO event bus so that it may reconnect the
        opcua client. Execute this in a different thread
        """
        while True:
            try:
                self._root_node.get_child(["0:Objects","0:Server","0:ServerStatus"])
                #node.get_value()
                self._is_connected = True
                time.sleep(10)
            except Exception:
                logging.error('Client disconnected from server')
                self._event.send('opcua_client_connection_status', {'content': 'disconnected'})

    def read_data(self, node_browsepaths):
        """Function to read data from an OPCUA Server
        Reads data from a list of browsepaths
        nodes -- a dict of nodes in string format, example:
        ["0:Objects", "2:Machine", "2:1"]
        """

        payload = {}

        for node in node_browsepaths:
            var = self._root_node.get_child(node)
            node_name = var.get_browse_name().__dict__['Name']
            node_value = var.get_value()
            payload.update({str(node_name): str(node_value)})

        logging.debug('Payload: ' + str(payload))
        self._event.send('opcua/periodic', {'content': payload})

    def read_data_deep(self, nodes):
        """
        Reads data from a set of nodes and goes up to two levels deep depending on the config.
        nodes -- a list of browse paths in string format, i.e. "2:1":"and some nested node"
        """

        payload = {'data': {}}
        try:
            message_dict = eval(str(nodes))
            for x in message_dict.keys():
                var = self._root_node.get_child(["0:Objects", "2:Machine", x])
                children = var.get_children()
                payload['data'].update({x: {}})
                if not message_dict[x]:
                    if(len(children) > 0):
                        for y in children:
                            payload['data'][x].update({y.get_browse_name().__dict__['Name']: str(y.get_value())})
                    else:
                        payload['data'][x].update({var.get_browse_name().__dict__['Name']: str(var.get_value())})
                else:
                    data = message_dict[x]
                    for y in children:
                        name = y.get_browse_name().__dict__['Name']
                        for z in data:
                            if name == z:
                                payload['data'][x].update({name: str(y.get_value())})
            logging.debug(payload)
            self._event.send('opcua/periodic', {'content': payload})
        except Exception as e:
            logging.error(e)
            logging.error('Bad payload specified in config')

    def write_data(self, nodes_and_values):
        """
        Writes data to a set of nodes.
        node_ids_and_values -- a dict of NodeIDs and their desired values
        # TODO: Implement this function
        """

        try:
            for node_id, value in nodes_and_values:
                self._client.get_node(node_id).set_value(value)
        except:
            logging.error('Writing values was not successful.')

    def browsepaths_to_nodeids(self, browsepaths):
        """
        Finds a node by its browsepath
        browsepath -- The browse name of the node that is to be found
        """

        nodeids = []
        for path in browsepaths:
            nodeids.append(self._root_node.get_child(path))
        return nodeids

    def create_subscription(self, period, browsepaths):
        """
        Creates a subscription on a list of node_ids
        browsepaths -- The nodes that are to be subscribed to
                    in string format, i.e. "ns=2;i=1"
        period   -- Subscription frequency
        """
        self._opcua_client_subscriptions = browsepaths
        self._opcua_client_subscription_period = period
        sub = self._client.create_subscription(period, self._handler)
        sub.subscribe_data_change(self.browsepaths_to_nodeids(browsepaths))

    def handle_data(self, message, topic):  # handles data from http
        """
        This method is called upon an event that the client
        is set up to respond to. #TODO: This needs to be universal
        """

        try:
            if message['command'] == 'GET':
                var = self._root_node.get_child(["0:Objects", "2:" + message['node']])
                children = var.get_children()
                for x in children:
                    if x.get_browse_name().__dict__['Name'] == message['name']:
                        logging.info('Gathering required data and sending event')
                        self._event.send('http/response', {'value': x.get_value(), 'target': message['source']})
                        logging.info('Server successfully processed eventdata')
            elif message['command'] == 'POST':
                try:
                    message_dict = message['payload']
                    for x in message_dict.keys():
                        var = self._root_node.get_child(["0:Objects", "2:" + x])
                        children = var.get_children()
                        payload_node = message_dict[x]
                        for y in payload_node.keys():
                            for z in children:
                                name = z.get_browse_name().__dict__['Name']
                                if name == y:
                                    z.set_value(payload_node[y])
                    logging.info('Server successfully processed eventdata')
                    self._event.send('http/response', {'status': '200', 'target': message['source']})
                    logging.info('Event has been sent.')
                except KeyError:
                    logging.error('Bad payload, sending event with error data...')
                    self._event.send('http/response', {'status': '402', 'target': message['source']})
                    logging.info('Event has been sent.')
                except:
                    logging.exception('')
        except KeyError:
            logging.debug('Unrelevant event detected in relevant topic, skipping...')
        except:
            logging.warning('SERVER COULD NOT PROCESS EVENTDATA')

    def datachange_notification(self, node, val, data):
        """
        Called when a value on the opcua server changes
        """
        sub_data = {'node': node.nodeid.Identifier, 'value': val}
        logging.debug('New OPCUA Data Change: ' + str(data))
        self._event.send('opcua/sub', {'content': sub_data})

    def event_notification(self, event):
        """
        This method is called if an subscribed to event is triggered.
        """

        try:
            parentNode = event.__dict__['ParentNode']
            logging.info('OPCUA: Subscribed Value has changed: ' + event.__dict__['Text'] + ' = ' + str(event.__dict__['Value']))
            logging.info('OPCUA: Sent from Object ' + parentNode)
            logging.info('OPCUA: Sending Event...')
            self._event.send("opcuaserver/events", {"event": event})
        except KeyError:
            logging.info('OPCUA: Triggervalue of object has changed, all values of object were returned:')
            data = event.get_event_props_as_fields_dict()
            array_info = ['EventId', 'EventType', 'SourceNode',
                          'SourceName', 'Time', 'ReceiveTime', 'LocalTime', 'Severity']
            for x in array_info:
                del data[x]  # remove unneccessary data from array
            for x in data:
                value = data[x].__dict__['Value']
                if value != '':
                    try:
                        logging.info(
                            ' ' + x + ': ' + value.__dict__['Text'])
                    except AttributeError:
                        logging.info(' ' + x + ': ' + str(value))
            logging.info('OPCUA: Sending Event...')
            self._event.send('opcuaserver/events', {'name': 'BaMo', 'event': event})
        except:
            logging.exception('')

    def stop_client(self):
        self._client.disconnect()
        logging.info('Client disconnected!')

    def handle_event(self, topic, properties):

        logging.debug('Got a {0} event "{1}" at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

        if (topic == 'opcua_client_connection_status'):
            if (properties['content'] == 'disconnected'):
                self._is_connected = False
                self._unregister_service()
                self.try_connection()

        if topic == 'rest_api/get_subbed':
           self._rest_api_svc.on_event_response(self._opcua_client_subscriptions)

        if topic == 'rest_api/get_sub_period':
            self._rest_api_svc.on_event_response(self._opcua_client_subscription_period)

    def read_config(self):
        """
        Reads config for client from json file
        """
        try:
            config = open('config/opcua_client_config.json')
            with config as f:
                data = json.load(f)
            return(data)
        except:
            logging.error('Error while parsing client config.')