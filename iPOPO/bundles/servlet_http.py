from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires, Validate

@ComponentFactory(name='http_service_factory')
@Instantiate('servlet_http')
@Provides(specifications='pelix.http.servlet')
@Property('_path', 'pelix.http.path', '/servlet')
class HTTP_Service(object):
    """
    Simple servlet factory, uses a servlet
    """

    def __init__(self):
        self._path = None

    def bound_to(self, path, params):
        """
        Servlet bound to a path
        """
        print('Bound to ' + path)
        return True

    def unbound_from(self, path, params):
        """
        Servlet unbound from a path
        """
        print('Unbound from ' + path)
        return None

    def do_GET(self, request, response):
        """
        Handle a GET
        """
        content = '<p>YEET</p>'.format(clt_addr=request.get_client_address(),
                  host=request.get_header('host', 0),
                  keys=request.get_headers().keys())

        response.send_content(200, content)
