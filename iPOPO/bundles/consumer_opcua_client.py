from pelix.ipopo.decorators import ComponentFactory, Requires, Provides, Property, \
    Validate, Invalidate, Instantiate
from threading import Thread
import logging
import os
import json
import time
import pelix.services


@ComponentFactory('opcua-client-consumer-factory')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Requires('_svc', 'opcua_client_service')
@Instantiate('consumer_opcua_client')
class OPCUA_Client_Consumer(object):

    def __init__(self):
        '''
        This method is called upon initiation of this script
        '''
        self._event_handler_topic = None
        self._event_handler_filter = None
        self._svc = None
        self._client_config = None
        self.condition = True  # used for periodic requests
        self.valid = False

    @Validate
    def validate(self, context):
        '''
        Validation for the component, called by the framework
        context -- bundle context
        '''
        self._client_config = self.read_config()
        self.valid = True
        logging.info('Client is connected')
        # Just for testing purposes, in the release version this should not be in validate
        # self._svc.read_data(self._client_config['periodic-nodes'])
        self._svc.create_subscription(self._client_config['period'], self._client_config['sub-nodes'])
        # TODO: Fix this bullshit. Find some easier way to periodically read nodes,
        #       if that's even neccessary.
        #self.t1 = Thread(target=self.run_loop)
        #self.t1.start()
        logging.info('OPCUA Consumer validated.')


    def run_loop(self):
        while self.condition:
            try:
                for _x in range(int(self._client_config['period'] * 2)):
                    if self.condition:
                        pass
                        time.sleep(0.5)
                    else:
                        # this will exit the for loop and try to execute next line, but that throws an exception
                        # No, that's not "good". See TODO above
                        break
                OPCUA_Client_Consumer.make_request(self)
            except Exception as e:
                logging.error(e)
                logging.info('Thread has been closed.')

    def read_config(self):
        '''
        Reads config for client from json file
        '''
        try:
            config = open('config/opcua_client_config.json')
            with config as f:
                data = json.load(f)
            return(data)
        except:
            logging.error('Error while parsing client config.')

    def make_request(self):
        '''
        Makes a request to the server
        '''
        logging.info('Period has ended, getting specified data.')
        self._svc.read_data(self._client_config['periodic-nodes'])

    def handle_event(self, topic, properties):
        '''
        Handles client events
        topic       -- The event topic
        properties  -- The event properties
        '''
        eventlist = self._client_config['topics']
        for event in eventlist:
            if topic == event:  # here use regex and get this stuff from config as well.
                logging.debug('Relevant event detected, processing...' + str(event))
                self._svc.handle_data(properties, topic)
            else:
                logging.debug('Event detected: Not relevant for this client, skipping. Event: ' + str(event))

    @Invalidate
    def invalidate(self, context):
        '''
        Invalidates the component
        context -- idk
        '''
        self.valid = False
        logging.info("INVALIDATED")
        #self.condition = False
        #if self.t1.is_alive():
        #    logging.warning('There are still threads connected to these bundles.')
        #    self._svc.stop_client()
        #    logging.info('Component invalidated, the service is gone.')
