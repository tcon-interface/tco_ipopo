from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires, Validate
import pelix.services
import sqlite3, json, logging, aiosqlite, asyncio


@ComponentFactory('handler_database_factory')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Instantiate('handler_database')
class DatabaseHandler(object):
    """
    A handler component for all database things
    in this project
    """

    def __init__(self):
        self._conn = None
        self._cursor = None
        self._event = None
        self._config = None

    @Validate
    def validate(self, context):
        self._config = self.read_config()
        self.init_db()

    async def init_db(self):
        """
        Initiates the DB
        """
        async with aiosqlite.connect('database.db') as db:
            for key, value in self._config['core-tables']:
                await db.execute(value)

    async def executeSQL(self, sql_command):
        """
        Executes a SQL command recieved via the event bus
        """
        async with aiosqlite.connect('database.db') as db:
            await db.execute(sql_command)

    def read_config(self):
        """
        Reads config for client from json file
        """
        try:
            config = open('config/db_config.json')
            with config as f:
                data = json.load(f)
            return(data)
        except:
            logging.error('Error while parsing client config.')

    def handle_event(self, topic, properties):
        """
        Scans each event matching with the filter defined in the annotations
        for a string, if that too is a match, something is executed
        """

        logging.debug('Got a {0} event "{1}" at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

        if topic == 'sqlite/sqlcommand':
            self.executeSQL(properties['content'])