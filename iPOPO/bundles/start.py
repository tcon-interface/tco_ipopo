'''
Starts a Pelix framework and installs the configured bundles
'''

from pelix.ipopo.constants import use_ipopo
import pelix.framework
import logging
import logging.config
import json


def main():

    # Read config from file
    bundles_to_start = json.load(open('config/framework_config.json'))['bundles_to_start']

    framework = pelix.framework.create_framework([
        'pelix.ipopo.core',    # iPOPO
        'pelix.shell.core',    # Shell core (engine)
        'pelix.shell.console'  # Text console
    ])

    framework.start()

    context = framework.get_bundle_context()

    for bundle in bundles_to_start:
        context.install_bundle(bundle).start()
        if bundle == 'pelix.services.eventadmin':
            with use_ipopo(context) as ipopo:
                ipopo.instantiate('pelix-services-eventadmin-factory', 'eventadmin', {})
        if bundle == 'servlet_http':
            with use_ipopo(context) as ipopo:
                ipopo.instantiate('pelix.http.service.basic.factory', 'http-server', {
                    'pelix.http.address':'localhost',
                    'pelix.http.port':'8886'
                })
    framework.wait_for_stop()


if __name__ == '__main__':
    logging.config.fileConfig('./config/logging.conf')
    logging.getLogger('requests').setLevel(logging.WARNING)
    logging.getLogger('opcua').setLevel(logging.WARNING)
    main()