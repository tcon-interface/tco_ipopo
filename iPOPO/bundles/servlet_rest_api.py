from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires, Validate
from pelix.http.routing import RestDispatcher, HttpGet, HttpPost, HttpPut
import pelix.services
import logging, uuid

@ComponentFactory(name='servlet_rest_api_factory')
@Instantiate('servlet_rest_api')
@Provides(specifications='pelix.http.servlet')
@Provides('servlet_rest_api_service')
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Property('_path', 'pelix.http.path', '/api/v0')
class REST_API_Servlet(RestDispatcher):
    """
    Simple servlet factory, uses a servlet
    """

    def __init__(self):
        super(REST_API_Servlet, self).__init__()
        self._path = None
        self._event = None
        self._event_response = None

    def bound_to(self, path, params):
        """
        Servlet bound to a path
        """
        print('Bound to ' + path)
        return True

    def unbound_from(self, path, params):
        """
        Servlet unbound from a path
        """
        print('Unbound from ' + path)
        return None

    def on_event_response(self, event_response):
        """
        This is called by the component that acutally gets the
        requested data. This is a 'rest api return' workaround,
        maybe this can be done in a better way.
        """
        self._event_response = event_response

    def handle_event(self, topic, properties):
        logging.debug('Got a {0} event "{1}" at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))


    @HttpGet('/subbednodes')
    def get_subbed_nodes(self, request, response):
        """
        REST API call: get a list of all currently subscribed
        to nodes
        """
        self._event.send('rest_api/get_subbed', {'content':''})
        logging.info('Content that would be returned here: ' + str(self._event_response))
        response.send_content(200, '<p>{}</p>'.format(str(self._event_response)))
        self._event_response = None

    @HttpGet('/subperiod')
    def get_sub_peroid(self, request, response):
        """
        REST API call: get a list of all currently subscribed
        to nodes
        """
        self._event.send('rest_api/get_sub_period', {'content':''})
        logging.info('Content that would be returned here: ' + str(self._event_response))
        response.send_content(200, '<p>{}</p>'.format(str(self._event_response)))
        self._event_response = None

    @HttpGet('/list')
    def list_elements(self, request, response):
        response.send_content(200, '<p>The list</p>')

    @HttpPost('/form/<form_id:uuid>')
    def handle_form(self, request, response, form_id):
        response.send_content(200, '<p>Handled {}</p>'.format(form_id))

    @HttpPut('/upload/<some_id:int>/<filename:path>')
    @HttpPut('/upload/<filename:path>')
    def handle_upload(self, request, response,some_id=None, filename=None):
        response.send_content(200, '<p>Handled {} : {}</p>'.format(some_id, filename))
