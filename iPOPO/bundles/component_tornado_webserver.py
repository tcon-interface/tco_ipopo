from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires, Validate
import pelix.services
import tornado.ioloop
from tornado.ioloop import IOLoop
import tornado.web
import logging, json
from threading import Thread
import asyncio

@ComponentFactory('tornado-webserver-factory')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Instantiate('component_tornado_webserver')
class Tornado_Webserver(object):

    def __init__(self):
        self._event_handler_topic = None
        self._event_handler_filter = None
        self._tornado_config = None
        self._app = None
        self._ioloop = None

    @Validate
    def validate(self, context):
        self._tornado_config = self.read_config()
        self._ioloop = tornado.ioloop.IOLoop.instance()
        tornado_thread = Thread(target=self.run_tornado)
        tornado_thread.daemon = True
        tornado_thread.start()
        logging.info("Tornado Webserver validated.")

    def run_tornado(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        ws = WebServer()
        ws.run()

    def handle_event(self, topic, properties):
        """
        Event received
        """
        logging.debug('Got a {0} event from {1} at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

    def read_config(self):
        '''
        Reads config for client from json file
        '''
        try:
            config = open('config/http_client_config.json')
            with config as f:
                data = json.load(f)
            return(data)
        except:
            logging.error('Error while parsing client config.')


class WebServer(tornado.web.Application):
    def __init__(self):
        handlers = [ (r"/test", MainHandler), ]
        settings = {'debug': True}
        super().__init__(handlers, **settings)

    def run(self, port=8886):
        self.listen(port)
        tornado.ioloop.IOLoop.instance().start()

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")