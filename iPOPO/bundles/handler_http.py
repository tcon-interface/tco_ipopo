from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires, Validate

import pelix.ipopo.constants as constants
import pelix.services
import json, http, logging, requests
from threading import Thread
from requests import get, post, put, patch, delete, options, head

@ComponentFactory('http-handler-factory')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Instantiate('handler_http')
class HTTP_Handler(object):
    """Handles events that want to send a HTTP request
    """

    def __init__(self):
        """
        Set up members
        """
        self._event_handler_topic = None
        self._event_handler_filter = None
        self._svc = None
        self._client_config = None
        self._request_methods = {
            'get': get,
            'post': post,
            'put': put,
            'patch': patch,
            'delete': delete,
            'options': options,
            'head': head,
        }

    @Validate
    def validate(self, context):
        self._client_config = self.read_config()
        logging.info('HTTP Component validated.')

    def handle_event(self, topic, properties):
        """
        Event received
        """
        logging.debug('Got a {0} event from {1} at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

        if topic == 'json_transformer':
            sap_client = '100'
            sap_user = self._client_config['CAT']['user']
            sap_pw = self._client_config['CAT']['password']
            url = self._client_config['CAT']['URL'] + '?sap-client=' + sap_client + '&sap-user=' + sap_user + '&sap-password=' + sap_pw
            url = self._client_config['CAT']['URL']
            try:
                self.async_request('post', url, data = properties['content'], callback=lambda r: logging.info(r))
            except requests.exceptions.Timeout:
                logging.info('TIMEOUT')
            except requests.exceptions.TooManyRedirects:
                logging.info('REDIRECT')
            except requests.exceptions.RequestException as e:
                logging.info(e)

    def read_config(self):
        '''
        Reads config for client from json file
        '''
        try:
            config = open('config/http_client_config.json')
            with config as f:
                data = json.load(f)
            return(data)
        except:
            logging.error('Error while parsing client config.')

    def async_request(self, method, *args, callback=None, timeout=15, **kwargs):
        """Makes request on a different thread, and optionally passes response to a
        `callback` function when request returns.
        """
        method = self._request_methods[method.lower()]
        if callback:
            def callback_with_args(response, *args, **kwargs):
                callback(response)
            kwargs['hooks'] = {'response': callback_with_args}
        kwargs['timeout'] = timeout
        thread = Thread(target=method, args=args, kwargs=kwargs)
        thread.start()