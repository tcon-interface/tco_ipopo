from pelix.ipopo.decorators import ComponentFactory, Requires, Provides, Instantiate, Property
import pelix.services
import pelix.framework
import logging
from logging.handlers import RotatingFileHandler

@ComponentFactory('component_logger_factory')
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Instantiate('component_logger')
class LoggerComponent(object):

    def __init__(self):
        self._event = None
        self._logger = logging.getLogger('eventLogger')
        self._handler = RotatingFileHandler('iPOPO_log.log', maxBytes=4000000, backupCount=10)
        self._logger.addHandler(self._handler)


    def handle_event(self, topic, properties):

        logging.debug('Got a {0} event "{1}" at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

        if (topic == 'opcua_client_connection_status'):
            self._logger.info(str(properties))

        if topic == 'opcua/sub':
            self._logger.info(str(properties['content']))
