from pelix.ipopo.decorators import ComponentFactory, Provides, Property, Instantiate, Requires

import pelix.services
import json
import datetime
import logging


@ComponentFactory('json-transformer-factory')
@Provides(pelix.services.SERVICE_EVENT_HANDLER)
@Requires('_event', pelix.services.SERVICE_EVENT_ADMIN)
@Property('_event_handler_topic', pelix.services.PROP_EVENT_TOPICS, ['*'])
@Property('_event_handler_filter', pelix.services.PROP_EVENT_FILTER)
@Instantiate('component_json_transformer')
class JSON_Transformer(object):
    """Transforms data from any bundle to a specified JSON format
    Subscribes to events from all bundles that want to send data
    Transforms data
    Publishes the data to other services that can send the data
    """

    def __init__(self):
        """
        Set up members
        """
        self._event_handler_topic = None
        self._event_handler_filter = None
        self._event = None

    def build_json_from_OPCUA_bundle(self, properties):
        """Method to build the JSON from the OPCUA Bundle
        properties -- Properties of the event from the opcua bundle
        """
        base_json = {'message': {'meta': '', 'data': ''}}
        meta = [
            {
                'meta_key': 'sender',
                'meta_value': 'OPCR'
            },
            {
                'meta_key': 'receiver',
                'meta_value': 'CAT'
            },
            {
                'meta_key': 'message_type',
                'meta_value': 'MA1SEK'
            },
            {
                'meta_key': 'bgrfc',
                'meta_value': 'false'
            }
        ]
        datalist = []
        for key, value in properties.items():
            tsp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
            datalist.append({'ext_id': key, 'tag_value_int': int(value), 'tag_tsp': tsp})
        base_json['message']['meta'] = meta
        base_json['message']['data'] = datalist
        logging.debug(base_json)
        return json.dumps(base_json)

    def build_json_from_OPCUA_bundle_sub(self, content):
        """Method to build the JSON from the OPCUA Bundle
        properties -- Properties of the event from the opcua bundle
        """
        base_json = {'message': {'meta': '', 'data': ''}}
        meta = [
            {
                'meta_key': 'sender',
                'meta_value': 'FISCHER'
            },
            {
                'meta_key': 'receiver',
                'meta_value': 'EC5'
            },
            {
                'meta_key': 'message_type',
                'meta_value': 'OPCUASUB'
            },
            {
                'meta_key': 'bgrfc',
                'meta_value': 'false'
            }
        ]
        tsp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-4]
        data = [{'ext_id': content['node'], 'tag_value_int': int(content['value']), 'tag_tsp': tsp}]
        base_json['message']['meta'] = meta
        base_json['message']['data'] = data
        logging.debug(base_json)
        return json.dumps(base_json)

    def handle_event(self, topic, properties):
        """
        Event received
        """
        logging.debug('Got a {0} event from {1} at {2}' .format(topic, properties['content'], properties[pelix.services.EVENT_PROP_TIMESTAMP]))

        if topic == 'opcua/sub':
            rjson = self.build_json_from_OPCUA_bundle_sub(properties['content'])
            self._event.send('json_transformer', {'content': rjson})